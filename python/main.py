import json
import socket
import sys

from collections import namedtuple

class Straight:
    def __init__(self, length, switch):
        self.length = length
        self.switch = switch

    def __repr__(self):
        return 'Straight: ' + str(self.length) + ' ' + str(self.switch)

class Bend:
    def __init__(self, radius, angle, switch):
        self.radius = radius
        self.angle = angle
        self.switch = switch
    
    def __repr__(self):
        return 'Bend: ' + str(self.radius) + ' ' + str(self.angle) + ' ' + str(self.switch)

class Track:
    def __init__(self, race):
        self.pieces = []
        for piece in race["track"]["pieces"]:
            if "length" in piece: # Only straight lines have a "length" attribute
                self.pieces.append(Straight(piece["length"], "switch" in piece))
            else:
                self.pieces.append(Bend(piece["radius"], piece["angle"], "switch" in piece))

        self.n_pieces = len(self.pieces)

        # Sort lanes so that the first lane is the rightmost one
        lanes = sorted(race["track"]["lanes"], key=lambda x: -x["distanceFromCenter"])

        # At the end, we have a map index -> position_from_leftmost_lane
        self.lanes = { lane["index"]: i for i, lane in enumerate(lanes) }
        self.max_lane = len(lanes) - 1

    # Highest sum of angles (absolute value) until next switch
    def best_direction(self, index):
        left_sum, right_sum = 0., 0.
        for i in xrange(2, len(self.pieces)):
            piece = self.pieces[(i + index) % len(self.pieces)]
            if isinstance(piece, Bend):
                if piece.angle > 0.:
                    right_sum += piece.angle
                else:
                    left_sum += -piece.angle           
            if piece.switch:
                break
        
        if left_sum > right_sum:
            return "Left"
        elif left_sum < right_sum:
            return "Right"
        return None

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.our_car, self.race, self.track = None, None, None
        self.switch_index = -1

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        print(data)
        self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def switching(self, car):
        lane = car["piecePosition"]["lane"]
        return lane["startLaneIndex"] != lane["endLaneIndex"], lane["startLaneIndex"]

    def msg_loop(self):
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if msg_type == 'yourCar':
                self.our_car = data
            elif msg_type == 'gameInit':
                self.race = data["race"]
                self.track = Track(self.race)
                print('\n'.join(map(str, self.track.pieces)))
            elif msg_type == 'carPositions':
                car = (c for c in data if c["id"] == self.our_car).next()
                index = car["piecePosition"]["pieceIndex"]

                
                if self.switch_index != -1 and index != self.switch_index:
                    self.switch_index = -1

                lane_index = car["piecePosition"]["lane"]["startLaneIndex"]
                msg_sent = False

                if self.switch_index == -1 and self.track.pieces[(index+1)%self.track.n_pieces].switch:
                    direction = self.track.best_direction(index)

                    if direction == 'Right' and lane_index < self.track.max_lane:
                        self.switch_lane("Right")
                        self.switch_index = index
                        msg_sent = True
                        print("Switch Right", index)
                    elif direction == 'Left' and lane_index > 0:
                        self.switch_lane("Left")
                        self.switch_index = index
                        msg_sent = True
                        print("Switch Left", index)
                
                if not msg_sent:
                    piece = self.track.pieces[(index+2)%self.track.n_pieces]
                    if isinstance(piece, Bend) and abs(piece.angle) > 40.:
                        self.throttle(0.37)
                        #print("Onooooooes")
                    else:
                        self.throttle(1.0)
                        #print("Yahooooooo")
            else:
                print(msg_type, data)

            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
