package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"strconv"
)

type piece struct {
	Type string
	Length float64
	Switch bool
	Radius float64
	Angle float64
}

type lane struct {
	index int
	distance int
}

type track struct {
	Pieces []piece
	Lanes []lane
}

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

/*
func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	switch msgtype {
	case "join":
		log.Printf("Joined")
		send_ping(writer)
	case "gameStart":
		log.Printf("Game started")
		send_ping(writer)
	case "crash":
		log.Printf("Someone crashed")
		send_ping(writer)
	case "gameEnd":
		log.Printf("Game ended")
		send_ping(writer)
	case "carPositions":
		send_throttle(writer, 0.6)
	case "error":
		log.Printf(fmt.Sprintf("Got error: %v", data))
		send_ping(writer)
	default:
		log.Printf("Got msg type: %s %v", msgtype, data)
		send_ping(writer)
	}
	return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil)
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}))
			if err != nil {
				return
			}
		}
	}
	return
}
*/

func parse_input(writer *bufio.Writer, input interface{}) (msgType string, data interface{}, err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		
		msgType = msg["msgType"].(string)
		
		switch msg["data"].(type) {
		default:
			log.Printf("%s: %v", msgType, data)
		case interface{}:
			data = msg["data"].(interface{})
		}
	}
	return
}

func parse_track(tr map[string]interface{}) (t track) {
	t.Pieces = make([]piece, len(tr["pieces"].([]interface{})))
	
	for i, p := range tr["pieces"].([]interface{}) {
		p := p.(map[string]interface{})
		
		if length, ok := p["length"].(float64); ok {
			t.Pieces[i].Type = "straight"
			t.Pieces[i].Length = length
			_, t.Pieces[i].Switch = p["switch"].(bool)
		} else {
			t.Pieces[i].Type = "curve"
			t.Pieces[i].Radius = p["radius"].(float64)
			t.Pieces[i].Angle = p["angle"].(float64)
			t.Pieces[i].Length = math.Abs(t.Pieces[i].Radius * (t.Pieces[i].Angle * math.Pi / 180.0)) // L = r * theta
		}
		fmt.Println(t.Pieces[i])
	}
	return
}

func get_car(data interface{}, our_color string) map[string]interface{} {
	for _, c := range data.([]interface{}) {
		car := c.(map[string]interface{})
		if car["id"].(map[string]interface{})["color"] == our_color {
			return car
		}
	}
	return nil
}

func bot_loop(conn net.Conn, name string, key string) (err error) {
	var msgType string
	var data interface{}
	
	var our_car map[string]interface{}
	var car map[string]interface{}
	
	var our_track track
	
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	
	send_join(writer, name, key)
	
	for {
		input, err := read_msg(reader)
		if err != nil {
			log_and_exit(err)
		}
		msgType, data, err = parse_input(writer, input)
		if err != nil {
			log_and_exit(err)
		}
		
		switch msgType {
		default:
			log.Printf("%s: %v", msgType, data)
		case "yourCar":
			our_car = data.(map[string]interface{})
		case "gameInit":
			our_track = parse_track(data.(map[string]interface{})["race"].(map[string]interface{})["track"].(map[string]interface{})) // JE CODE AVEC LE CUL
		case "carPositions":
			//log.Printf("%v", data)
			car = get_car(data, our_car["color"].(string))
			//log.Println(car["angle"])
			
			index := int(car["piecePosition"].(map[string]interface{})["pieceIndex"].(float64)) // LALALA LALA LALA
			
			if index + 2 < len(our_track.Pieces) && our_track.Pieces[index+2].Type == "curve" {
				if math.Abs(our_track.Pieces[index+2].Angle) > 40 {
					//log.Println("Onoooooooes")
					send_throttle(writer, 0.425)
				} else {
					//log.Println("Re-Yahooooo")
					send_throttle(writer, 1.0)
				}
				
				//log.Println("Onoeeeees")
				//log.Println(car["piecePosition"].(map[string]interface{})["inPieceDistance"])
				//log.Println(car["piecePosition"].(map[string]interface{})["pieceIndex"])
			} else {
				send_throttle(writer, 1.0)
				//log.Println("Yahoooooo")
			}
			
			//log.Printf("%v", our_car)
			//send_throttle(writer, 0.5)
			//log.Printf("%s: %v", msgType, data)
		}
	}
	
	return
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

func log_and_exit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func main() {

	host, port, name, key, err := parse_args()

	if err != nil {
		log_and_exit(err)
	}

	fmt.Println("Connecting with parameters:")
	fmt.Printf("host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)

	conn, err := connect(host, port)

	if err != nil {
		log_and_exit(err)
	}

	defer conn.Close()

	err = bot_loop(conn, name, key)
}
